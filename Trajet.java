public class Trajet {
    Station dept;
    Station arriv;
    int duree;

    public Trajet(Station dept, Station arriv, int duree) {
        this.dept = dept;
        this.arriv = arriv;
        this.duree = duree;
    }

    public Station getDept() {
        return dept;
    }

    public Station getArriv() {
        return arriv;
    }

    public int getDuree() {
        return duree;
    }
}
