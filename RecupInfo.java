
import java.io.*;
import java.text.ParseException;
import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Pattern;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class RecupInfo {


    public RecupInfo()
    {

    }

    public void recupInfoTrain(File file) throws DOMException, ParseException, ParserConfigurationException, IOException, SAXException {
        ArrayList<LiaisonSimple> listTrain = new ArrayList<>();
        DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
        int numberLine = 1;
        Station arrivee = null;
        Station depart = null;
        LocalTime hDept = null;
        LocalTime hArriv =null ;
        int duree = 0;
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HHmm");

        DocumentBuilder builder = fact.newDocumentBuilder();
        Document doc = builder.parse(file);
        NodeList listLiaison = doc.getElementsByTagName("line");
        for (int i = 0; i < listLiaison.getLength(); i++) {
            Node l = listLiaison.item(i);
            if (l.getNodeType() == Node.ELEMENT_NODE) {
                Element line = (Element) l;
                NodeList subLine = line.getChildNodes();
                numberLine++;
                for (int j = 0; j < subLine.getLength(); j++) {
                    Node n = subLine.item(j);
                    if (n.getNodeType() == Node.ELEMENT_NODE) {
                        Element sub = (Element) n;
                        NodeList sub2Line = sub.getChildNodes();
                        for (int k = 0; k < sub2Line.getLength(); k++) {
                            Node m = sub2Line.item(k);
                            if (m.getNodeType() == Node.ELEMENT_NODE) {
                                Element sub2 = (Element) m;
                                if (sub2.getTagName() == "arrival-station")
                                    arrivee = Station.valueOf(sub2.getTextContent());
                                if (sub2.getTagName() == "start-station")
                                    depart = Station.valueOf(sub2.getTextContent());
                                if (sub2.getTagName() == "arrival-hour") {
                                    hArriv = LocalTime.parse(sub2.getTextContent(), dtf);
                                    duree = (int) (Duration.between(hDept, hArriv).toMinutes());
                                }
                                if (sub2.getTagName() == "start-hour")
                                {
                                    hDept = LocalTime.parse(sub2.getTextContent(),dtf);
                                    System.out.println("isaaaa" +hDept);

                                }
                                /**Maintenant On peut instancier notre classe avec les données récuprées*/
                                LiaisonSimple ls = new LiaisonSimple(depart, arrivee, hDept, hArriv, Exploitant.train);
                                ls.setDuree(duree);
                                listTrain.add(ls);
                                System.out.println(ls.toString());
                            }
                        }
                    }
                }
            }

        }


    }




    public void recupInfoTram (File file) throws ParserConfigurationException, IOException, SAXException, ParseException {
        ArrayList<LiaisonSimple> listTrain = new ArrayList<>();
        DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
        int numberLine = 1;
        Station arriv = null;Station depart = null;
        LocalTime hDept = null;LocalTime hArriv = null;
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HHmm");


        DocumentBuilder builder = fact.newDocumentBuilder();
        Document doc = builder.parse(file);
        NodeList line= doc.getElementsByTagName("lignes");
        for (int i = 0; i < line.getLength(); i++) {
            Node l1 = line.item(i);
            if (l1.getNodeType() == Node.ELEMENT_NODE) {
                Element e1 = (Element) l1;
                NodeList subLine1 = e1.getChildNodes();
                for (int j = 0; j < subLine1.getLength();j++)
                {
                    Node l2 = subLine1.item(j);
                    if(l2.getNodeType() == Node.ELEMENT_NODE)
                    {
                        Element e2 = (Element) l2;

                        NodeList subLine2 = e2.getChildNodes();

                        String [] listStations = subLine2.item(1).getTextContent().split("\\s");
                        for (int k = 2; k < subLine2.getLength();k++)
                        {
                            Node l3 = subLine2.item(k);
                            if(l3.getNodeType() == Node.ELEMENT_NODE)
                            {
                                String [] listHoraires = l3.getTextContent().split("\\s");
                                for (int p = 0; p < listStations.length-1;p++)
                                {
                                    LiaisonSimple ls = new LiaisonSimple(Exploitant.tram);
                                    ls.setDepart(Station.valueOf(listStations[p]));
                                    ls.setArrivee(Station.valueOf(listStations[p]));
                                    ls.setDuree(Integer.parseInt(listHoraires[p+1] )- (Integer.parseInt(listHoraires[p] )));
                                    ls.setDateDepart(LocalTime.parse(listHoraires[p],dtf));
                                    ls.setDateArrivee(LocalTime.parse(listHoraires[p+1],dtf));
                                    System.out.println(ls.toString());
                                }
                            }



                        }

                    }
                }
            }}



    }

    public void recupInfoMetroTxt(File file) throws IOException {
        ArrayList < Trajet> listTrajet= new ArrayList<>();
        ArrayList < LiaisonSimple> liaisonMetro = new ArrayList<>(); /**Nous allons initier une liste de liaison qui auront comme parametre que la station de depart, d'arrivée et la duréé*/
        /** Nous allons donc revenir sur la liste pour donner les heurs de départ et d'arrivée déduite de la durée*/
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HHmm");
        LocalTime departTime=null, lastDepartTime =null;

        int Frequency=0;
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String st;

            /** Vu que les fichiers txt ont en commun qu'ils contiennent les informations: Depart arrivee duree et heure de depart, mais écrit sous des formats différents
             nous allons
             on recupere le dernier élément de la liste*/

            while((st=br.readLine()) != null)
            {
                Trajet tj = new Trajet();
                if( Pattern.compile("^[a-zA-Z]+\\s+[a-zA-Z]+\\s+\\d\\d+$", Pattern.CASE_INSENSITIVE).matcher(st).matches()) // On verfie si la ligne est sous le format Station depart station arrive et duree
                /**ici on a supposé que la durée s'écrivait sur 2 chiffres*/
                {
                    String [] tb=st.split("\\s+");tj.setDept(Station.valueOf(tb[0]));tj.setArriv(Station.valueOf(tb[1]));tj.setDuree(Integer.parseInt(tb[2]));listTrajet.add(tj);
                    continue;
                }
                if(st.contains("partir")) {st = br.readLine();departTime = LocalTime.parse(st,dtf);continue;}
                if(st.contains("toutes les")) {st = br.readLine();Frequency = Integer.parseInt(st);continue;}
                if(st.contains("dernier")) {st = br.readLine();lastDepartTime = LocalTime.parse(st,dtf);continue;}


            } br.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        /**Maintenant on doit déduire les liaisons avec les heures de départ et les durées */
        Calendar cal = Calendar.getInstance();
        while (departTime.getHour()!= lastDepartTime.getHour() && departTime.getMinute()!= lastDepartTime.getMinute())

        {
            for(Trajet tj: listTrajet)
            {
                LiaisonSimple ls = new LiaisonSimple(Exploitant.metro);
                ls.setDepart(tj.getDept());
                ls.setArrivee(tj.getArriv());
                /** On recupère l'heure de départ*/
                ls.setDateDepart(departTime);
                ls.setDuree(tj.getDuree());
                /** Maintenant il faut déduire l'heure d'arrivée en utilisant la durée*/
                ls.setDateArrivee(ls.getDateDepart().plusMinutes(ls.duree));
                System.out.println(ls.toString()+ " " + lastDepartTime);
                /** Et finalement mettre à jour la valeur de departTime**/
                departTime = departTime.plusMinutes(Frequency);




            }

        }
    }




    public void recupInfoIntercitesTxt (File file )
    {
        ArrayList <LiaisonSimple> listIntercites = new ArrayList<>();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HHmm");
        String st;// le buffer qui permet de parcourir les lignes
        /** Pour ce fichier nous aurons besoins de recuperer les durées pour chaque ville, avec cela nous déduirons l'heure d'arrivée en faison une addition avec l'heure de depart*/
        Map < Integer, Map.Entry< String,String>> dataSet = new HashMap< Integer, Map.Entry< String,String>>(); // on crée un hashmap qui contient la durée (en integer), la station de part puis la station d'arrivéels

        //dataSet.containsValue()
        ArrayList <Trajet> listTrajet = new ArrayList<>();
        Station dept=null, arriv = null;
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            while( (st= br.readLine()) != null)
            {
                if( Pattern.compile("^[a-zA-Z]+\\s+[a-zA-Z]+\\s+\\d\\d", Pattern.CASE_INSENSITIVE).matcher(st).matches())
                {
                    String [] tb=st.split("\\s+");

                    dept = Station.valueOf(tb[0]);

                    arriv = Station.valueOf((tb[1]));
                    int duree = Integer.parseInt(tb[2]);
                    Trajet trajet = new Trajet(dept,arriv,duree);
                    listTrajet.add(trajet);
                    continue;
                }
                if( Pattern.compile("^[a-zA-Z]+\\s+[a-zA-Z]+\\s+\\d\\d\\d\\d+$", Pattern.CASE_INSENSITIVE).matcher(st).matches())
                {
                    LiaisonSimple ls = new LiaisonSimple(Exploitant.bus);
                    String [] tb=st.split("\\s+");

                    ls.setDepart(Station.valueOf(tb[0]));ls.setArrivee(Station.valueOf(tb[1]));ls.setDateDepart(LocalTime.parse(tb[2],dtf));
                    listIntercites.add(ls);
                }
            }
            /** Maintenant on deduit les heures d'arrivée**/
            Calendar cal = Calendar.getInstance();
            for( LiaisonSimple ls: listIntercites)
                for (Trajet tj : listTrajet) {

                    if ((ls.getArrivee() == tj.getArriv() && ls.getDepart() == tj.getDept()) || (ls.getArrivee()== tj.getDept() && ls.getDepart() == tj.getArriv())) {

                        ls.setDateArrivee(ls.getDateDepart().plusMinutes(tj.getDuree()));
                        ls.setDuree(tj.getDuree());

                    }

                }
            for( LiaisonSimple ls: listIntercites)
                System.out.println(ls.toString());


        } catch (IOException e) {
            e.printStackTrace();
        }

    }




    public static void main(String [] args) throws ParseException, ParserConfigurationException, IOException, SAXException {
        RecupInfo ri = new RecupInfo();
        File file =new File("src/metro.txt");
        //ri.recupInfoMetroTxt(file);
        File file2 =new File("src/InterCites.txt");
        ri.recupInfoIntercitesTxt(file2);










    }

    

}