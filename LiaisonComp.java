import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;



public class LiaisonComp  {
    private ArrayList<LiaisonSimple> trajets;
    /** origine */
    private Station depart;
    /** destination */
    private Station arrivee;
    /** duration de la journée, en minutes*/
    private int dureeTotale;
    /** date  de départ */
    private LocalTime dateDepart;
    /** date d'arrivée, format hhmm */
    private LocalTime dateArrivee;



    public LiaisonComp() {
        trajets = new ArrayList<>();
    }

    /**

     *
     * @param nouveau

     */

    public boolean add(LiaisonSimple nouveau) {

        boolean ajout=false;
        LiaisonSimple dernier = null;
        if(trajets==null || trajets.isEmpty()) {
            if(trajets==null) trajets = new ArrayList<>();
            depart = nouveau.depart;
            dateDepart = nouveau.dateDepart;
            ajout=true;
        }
        else
        {
            dernier = trajets.get(trajets.size()-1);
            if(dernier.getArrivee() == nouveau.getArrivee() && dernier.dateArrivee.isBefore(nouveau.dateDepart))
                ajout = true;
        }
        if(ajout)
        {
            trajets.add(nouveau);
            arrivee = nouveau.getArrivee();
            dateArrivee = nouveau.dateArrivee;
            dureeTotale += nouveau.duree;

        }
        return ajout;
    }


    public void add(List<LiaisonSimple> listeTrajets) {
        trajets.addAll(listeTrajets);
        calcule();
    }


    private void calcule()
    {
        LiaisonSimple premier = trajets.get(0);
        LiaisonSimple dernier = trajets.get(trajets.size()-1);
        depart = premier.getArrivee();
        arrivee = dernier.getDepart();
        dureeTotale = (int) ChronoUnit.MINUTES.between( premier.dateDepart, dernier.dateArrivee );

    }

    /**retourne la duree totale*/
    public int getDureeTotale() {return dureeTotale;}



    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder("trajet compose, ");
        sb.append("duree=").append(dureeTotale);

        String tirets = "---";
        trajets.forEach(t->sb.append(tirets).append(t).append("\n"));
        return sb.toString();
    }


    public static void main(String[] args) {
        LiaisonComp trajets = new LiaisonComp();


    }

}