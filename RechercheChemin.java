import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;



public class RechercheChemin {
    Map<Station, List<LiaisonSimple>> catalogue;

    RechercheChemin() { catalogue = new EnumMap<>(Station.class);}

    void addTrajetSimple(LiaisonSimple trajet) {
        catalogue.compute(trajet.depart,
                (v, l) -> {if(l==null) {l = new ArrayList<>();} l.add(trajet); return l;});
    }


    List<LiaisonSimple> trouveCheminsDirects(Station depart,Station arrivee, LocalTime dateDepart, int delaiMax) {
        List<LiaisonSimple> cheminsDirects = null;
        List<LiaisonSimple > trajets = catalogue.get(depart);
        if (trajets != null)
        {
            cheminsDirects = new ArrayList<>(List.copyOf(trajets));
            LocalTime dateDepartMax = dateDepart.plusMinutes(delaiMax);
            cheminsDirects.removeIf(t->(t.getArrivee() != arrivee  || t.dateDepart.isBefore(dateDepart) || t.dateDepart.isAfter(dateDepartMax) ) );
            if (cheminsDirects.isEmpty()) cheminsDirects = null;
        }
        return cheminsDirects ;
    }



    public boolean trouverCheminIndirect(Station depart, Station arrivee, LocalTime momentDepart, int delai, List<LiaisonSimple> voyageEnCours,
                                         List<Station> via, List<LiaisonComp> results) {
        boolean result;
        via.add(depart);
        //recherche des trajets à partir de depart
        List<LiaisonSimple> liste = new ArrayList<>(catalogue.get(depart));
        if (liste.isEmpty()) return false;
        //calcul de la date de depart au plus tard
        LocalTime dateDepartMax = momentDepart.plusMinutes(delai);
        //retrait des trajets partant trop tot ou trop tard
        liste.removeIf(t->(t.dateDepart.isBefore(momentDepart))||t.dateDepart.isAfter(dateDepartMax));
        for (LiaisonSimple t : liste) {
            //si on trouve un trajet menant à l'arrivée
            if (t.arrivee == arrivee) {
                //on l'ajoute au voyage en cours
                voyageEnCours.add(t);
                //on cree un nouveau trajet compose reprenant le detail du voyage
                LiaisonComp compo = new LiaisonComp();
                compo.add(List.copyOf(voyageEnCours));
                //on l'ajoute au resultat
                results.add(compo);
                //on retire le dernier trajet pour éventuellement en cherche un autre (plus rapide, moins cher, ...)
                voyageEnCours.remove(voyageEnCours.size() - 1);
            } else {
                //si le trajet ne mène pas à l'arrivee mais donc à un via
                if (!via.contains(t.arrivee)) {
                    //on l'ajoute au voyage en cours
                    voyageEnCours.add(t);
                    //on cherche à partir du via vers l'arrivee
                    trouverCheminIndirect(t.arrivee, arrivee, t.dateArrivee, delai, voyageEnCours, via, results);
                    //on retire les derniers ajouts pour chercher d'autres chemins
                    via.remove(t.arrivee);
                    voyageEnCours.remove(t);
                }
            }
        }
        result = !results.isEmpty();
        return result;
    }

    public static void main(String[] args) {

    }
}