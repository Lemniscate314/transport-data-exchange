import java.time.LocalTime;
import java.util.Date;

public class LiaisonSimple {
    protected Station depart;
    protected Station arrivee;
    protected LocalTime dateDepart;
    protected LocalTime dateArrivee;
    protected Exploitant exploitant;
    protected int duree;

    public LiaisonSimple ()
    {
        this.arrivee = null;
        
    }

    public LiaisonSimple(Station depart, Station arrivee, LocalTime dateDepart, LocalTime dateArrivee, Exploitant exploitant) {
        this.depart = depart;
        this.arrivee = arrivee;
        this.dateDepart = dateDepart;
        this.dateArrivee = dateArrivee;
        this.exploitant = exploitant;
    }

    public LiaisonSimple (Exploitant exploitant)
    {
        this.exploitant = exploitant;
    }


    public Station getArrivee() {
        return arrivee;
    }

    public Station getDepart() {
        return depart;
    }

    public LocalTime getDateDepart() {
        return dateDepart;
    }

    public LocalTime getDateArrivee() {
        return dateArrivee;
    }

    public Exploitant getExploitant() {
        return exploitant;
    }

    public int getDuree() {
        return duree;
    }

    public void setArrivee(Station arriv) {
        this.arrivee = arriv;
    }

    public void setDepart(Station depart) {
        this.depart = depart;
    }

    public void setDateDepart(LocalTime dateDepart) {
        this.dateDepart = dateDepart;
    }

    public void setDateArrivee(LocalTime dateArrivee) {
        this.dateArrivee = dateArrivee;
    }

    @Override
    public String toString() {
        return "LiaisonSimple{" +
                "depart=" + depart +
                ", arrivee=" + arrivee +
                ", dateDepart=" + dateDepart +
                ", dateArrivee=" + dateArrivee +
                ", exploitant=" + exploitant +
                ", duree=" + duree +
                '}';
    }

    public void setExploitant(Exploitant exploitant) {
        this.exploitant = exploitant;
    }

    public void setDuree(int duree) {
        this.duree = duree;
    }
}
